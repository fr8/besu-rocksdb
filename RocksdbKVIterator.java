TypeInformation<Integer> resultType = TypeExtractor.createTypeInfo(Integer.class);
TypeSerializer<Integer> serializer = resultType.createSerializer(new ExecutionConfig());

RocksIterator iterator =  db.newIterator(columnFamilyHandle);
iterator.seekToFirst();
iterator.status();

while (iterator.isValid()) {
    byte[] key = iterator.key();
    System.out.write(key);
    System.out.println(serializer.deserialize(new TestInputView(iterator.value())));
    iterator.next();
}